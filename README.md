# Trajectory Controller
This project is as a part of Adaptive and Optimal Control course EE6412. 

Aim is to develop a novel trajectory tracking controller using Optimal control techniques. 

## Applications 
This controller is made to be model independent hence, it can be applied for both Aerial Vehicles like Quadrotor and mobile robot.

##### To Do's:
1. ACADO toolkit 
2. Basic Framework
3. Model Header files 
4. Simulations
 
